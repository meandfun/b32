# -*- coding: utf-8 -*-
import time
import RPi.GPIO as GPIO
import sys

#Déclaration d'un dictionnaire des symboles
d = {}
d['a'] = '.-'
d['b'] = '-...'
d['c'] = '-.-.'
d['d'] = '-..'
d['e'] = '.'
d['f'] = '..-.'
d['g'] = '--.'
d['h'] = '....'
d['i'] = '..'
d['j'] = '.---'
d['k'] = '-.-'
d['l'] = '.-..'
d['m'] = '--'
d['n'] = '-.'
d['o'] = '---'
d['p'] = '.--.'
d['q'] = '--.-'
d['r'] = '.-.'
d['s'] = '...'
d['t'] = '-'
d['u'] = '..-'
d['v'] = '...-'
d['w'] = '.--'
d['x'] = '-..-'
d['y'] = '-.--'
d['z'] = '--..'
d[' '] = ' '
d['1'] = '.----'
d['2'] = '..---'
d['3'] = '...--'
d['4'] = '....-'
d['5'] = '.....'
d['6'] = '-....'
d['7'] = '--...'
d['8'] = '---..'
d['9'] = '----.'
d['0'] = '-----'

#Déclaration des durées (en ms)
noPine = int(sys.argv[1])
POINT = 0.2
TIRET = 3*POINT
ETEINTDANSLETTRE = POINT
ESPACELETTRE = 3*POINT
ESPACEMOTS = 7*POINT-ESPACELETTRE
#Caractère quitter
QUITTER = '-1'
ON  = 1
OFF = 0

def initialisationGPIO(noPine):
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(noPine,GPIO.OUT)

def lightSwitch(noPine, dureeSleep, OnOff):
        if OnOff == 1:         
                GPIO.output(noPine,GPIO.HIGH)
                time.sleep(dureeSleep)
        else:
                GPIO.output(noPine,GPIO.LOW)
                time.sleep(dureeSleep)

	
def traductionMorse():
	chaine = input("Veuillez entrer un mot ou une phrase S.V.P. ")
	print("Ce mot ou cette phrase sera maintenant convertie en code MORSE")
	listeMorse = []
	i = 0
	#On utilise chaque caractère de la chaine comme une clé du dictionnaire morse
	while (i< len(chaine)):
		uneLettre = ''
		uneLettre = d[chaine[i]]
		#On forme une liste des valeurs en morse 
		listeMorse.append(uneLettre)
		i += 1
	return listeMorse 
	

def lectureLettre(morseLettre):
	for i in range(len(morseLettre)):
                #Ouverture et fermeture de la lumière selon la chaîne reçue
		if (morseLettre[i] == '-'):
                        lightSwitch(noPine, TIRET, ON)
		elif (morseLettre[i] == '.'):
                        lightSwitch(noPine, POINT, ON)
		if (len(morseLettre)-i == 1):
			lightSwitch(noPine, ESPACELETTRE, OFF)
		else:
			lightSwitch(noPine, ETEINTDANSLETTRE, OFF)


def main():
        reponse = ''
        while reponse != QUITTER:
                try:
                        listeMorse = traductionMorse()
                        print(listeMorse)
                        initialisationGPIO(noPine)
                        for i in range(len(listeMorse)):
                                if (listeMorse[i] == ' '):
                                        time.sleep(ESPACEMOTS)
                                else:
                                        lectureLettre(listeMorse[i])
                except:
                        GPIO.output(noPine,GPIO.LOW)
                reponse = input("Désirez-vous continuer ? (-1 pour quitter) ")
main()

	
