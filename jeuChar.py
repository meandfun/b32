#-*- coding: utf-8 -*-
 
 # utiliser blink.py a la place du print en fonction
 
import sys
import os
import time
import random
import RPi.GPIO as GPIO

DELAI = 1

def sequenceDepart():
	 os.system("clear")
	 print("Préparez-vous")
	 time.sleep(DELAI)
	 os.system("clear")
	 print(3)
	 time.sleep(DELAI)
	 os.system("clear")
	 print(2)
	 time.sleep(DELAI)
	 os.system("clear")
	 print(1)
	 time.sleep(DELAI)
	 os.system("clear")
	 print("Go!")
	 time.sleep(DELAI)
	 os.system("clear")
	 
def lightOn():
	GPIO.output(4,GPIO.HIGH)

def lightOff():
	GPIO.output(4,GPIO.LOW) 

def jouer(n,delai):
	os.system("clear")
	total=0.0
	for i in range(n):
		time.sleep(random.random() * delai)
		lightOn()
		t = time.time()
		input()
		lightOff()
		t = time.time() - t
		total += t
		print("Temps: ", t)
	print("Temps moyen: ", total/n)
		

def main():
	n = int(sys.argv[1])
	delai = float(sys.argv[2])
	
	GPIO.setmode(GPIO.BCM)
	GPIO.setwarnings(False)
	GPIO.setup(4,GPIO.OUT)
	
	sequenceDepart()
	jouer(n,delai)
	
	return 0
	
main()
